import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import {  faGaugeHigh,faUserEdit, faTag, faReceipt, faHandHoldingDollar, faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



export default function AppSideNavBar(){
    return(
      <Navbar  expand="lg" className='sidebar' bg='white'>
         
       <Navbar.Toggle aria-label='test'/>
       <Navbar.Collapse>
        <Nav className='flex-column'>
        <Navbar.Brand>B156 Admin</Navbar.Brand>
            <Link to="/" className='nav-link border-bottom'><FontAwesomeIcon icon={faGaugeHigh}></FontAwesomeIcon> Main dashboard</Link>
               <Link to="/courses" className='nav-link border-bottom'><FontAwesomeIcon icon={faTag}></FontAwesomeIcon> Product</Link>
               <Link to="/courses/add-course" className='nav-link border-bottom '><FontAwesomeIcon icon={faReceipt}></FontAwesomeIcon> Orders</Link>
               <Link to="/courses/add-course" className='nav-link border-bottom '><FontAwesomeIcon icon={faUserEdit}></FontAwesomeIcon> Customer</Link>
               <Link to="/register" className='nav-link border-bottom'> <FontAwesomeIcon icon={faHandHoldingDollar}></FontAwesomeIcon>Sales</Link>
               <Link to="/logout" className='nav-link border-bottom '><FontAwesomeIcon icon={faPenToSquare}></FontAwesomeIcon> Change Password</Link>
       
      </Nav>
      </Navbar.Collapse>
      </Navbar>
        );
    };
 
    
    